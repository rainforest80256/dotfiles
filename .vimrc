""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                  vim-plug                                  "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

call plug#begin('~/.local/share/nvim/plugged')

"""""""""""""""""
"  colorscheme  "
"""""""""""""""""
Plug 'whatyouhide/vim-gotham'
" airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"""""""""""
"  fonts  "
"""""""""""
" FiraCode
Plug 'tonsky/firacode'

" tagbar
Plug 'majutsushi/tagbar'


Plug 'ervandew/supertab'

Plug 'Valloric/YouCompleteMe'
"syntastic
Plug 'scrooloose/syntastic'

" Track the engine.
Plug 'SirVer/ultisnips'
" Snippets are separated from the engine.
Plug 'honza/vim-snippets'

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" 註釋工具
Plug 'scrooloose/nerdcommenter'

" AutoPairs
Plug 'jiangmiao/auto-pairs'

" indentLine
Plug 'Yggdroot/indentLine'

" vim-better-whitespace
Plug 'ntpeters/vim-better-whitespace'

" tabular排版 -> 須放vim-markdown前
Plug 'godlygeek/tabular'

" 折疊代碼
Plug 'tmhedberg/SimpylFold'

"""""""""
"  web  "
"""""""""
" html
"Emmet
Plug 'mattn/emmet-vim'

" pug
Plug 'digitaltoad/vim-pug'
" vim-pug-complete
Plug 'dNitro/vim-pug-complete', { 'for': ['jade', 'pug'] }

" JavaScript
Plug 'othree/javascript-libraries-syntax.vim'
" Vuejs
Plug 'posva/vim-vue'

""""""""""""
"  Python  "
""""""""""""
Plug 'vim-scripts/indentpython'
" PEP8代碼風格
Plug 'nvie/vim-flake8'
" jedi-vim
Plug 'davidhalter/jedi-vim'

""""""""""""""""""""""
"  LaTeX & Markdown  "
""""""""""""""""""""""
" markdown
Plug 'plasticboy/vim-markdown'

"vim-latex
Plug 'vim-latex/vim-latex'
"vim-latex-live-preview
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }

""""""""""""""""""""
"  Flutter & Dart  "
""""""""""""""""""""
" dart-vim-plugin
Plug 'dart-lang/dart-vim-plugin'

"""""""""""""
"  Backend  "
"""""""""""""
" json
Plug 'elzr/vim-json'
call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              vim-plug setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""
"  airline  "
"""""""""""""
" set status line
set laststatus=2
" enable powerline-fonts
let g:airline_powerline_fonts = 1
" enable tabline
let g:airline#extensions#tabline#enabled = 1
" set left separator
let g:airline#extensions#tabline#left_sep = ' '
" set left separator which are not editting
let g:airline#extensions#tabline#left_alt_sep = '|'
" colorscheme for airline
let g:airline_theme='onedark'


""""""""""""""
"  SuperTab  "
""""""""""""""
let g:SuperTabDefaultCompletionType    = '<C-n>'
let g:SuperTabCrMapping                = 0
"""""""""""""""
"  Ultisnips  "
"""""""""""""""
" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
"""""""""""""""""""
"  YouCompleteMe  "
"""""""""""""""""""
" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'
" 讓自動補全的視窗不會消失
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g :YcmCompleter GoToDefinitionElseDeclaration<CR>


"""""""""""""""
"  syntastic  "
"""""""""""""""
" 设置错误符号
let g:syntastic_error_symbol='✗'
" 设置警告符号
let g:syntastic_warning_symbol='⚠'
" 是否在打开文件时检查
let g:syntastic_check_on_open=0
" 是否在保存文件后检查
let g:syntastic_check_on_wq=1
let g:syntastic_pug_checkers = ['pug_lint']
""""""""""""""""""""
"  NERD Commenter  "
""""""""""""""""""""
" 注释的时候自动加个空格, 强迫症必配
let g:NERDSpaceDelims=1

""""""""""""""""
"  indentLine  "
""""""""""""""""
"缩进指示线"
let g:indentLine_char='┆'
let g:indentLine_enabled = 1

"""""""""""""
"  Folding  "
"""""""""""""
" SimplyFold
" see the docstrings for folded code
let g:SimpylFold_docstring_preview=1

"""""""""
"  web  "
"""""""""
au BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \| set softtabstop=2
    \| set shiftwidth=2

""""""""""""
"  Python  "
""""""""""""
au BufNewFile,BufRead *.py
	\ set fileformat=unix
    \| set encoding=utf-8

let python_highlight_all = 1

""""""""""""""""""""""
"  LaTeX & Markdown  "
""""""""""""""""""""""
" vim-markdown
let g:vim_markdown_no_default_key_mappings = 1
let g:tex_conceal = ""
let g:vim_markdown_conceal = 0
let g:vim_markdown_math = 1

" vim-latex-live-preview
autocmd Filetype tex setl updatetime=1
let g:livepreview_previewer = 'open -a texshop'

""""""""""""""""""""
"  Flutter & Dart  "
""""""""""""""""""""
" dart-vim-plugin
let dart_html_in_string=v:true
let dart_style_guide = 2
let dart_format_on_save = 1

"""""""""""""
"  Backend  "
"""""""""""""
" json
" elzr/vim-json
let g:vim_json_syntax_conceal = 0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                  General                                   "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""
"  color & colorscheme  "
"""""""""""""""""""""""""
"color
set t_Co=256
syntax on
" colorscheme
colo gotham256

"""""""""""""""""""
"  line & column  "
"""""""""""""""""""
set cursorline
set cursorcolumn
set nu
set relativenumber
set ruler
set mouse=a
" enable folding
set foldmethod=indent
set foldlevel=99
nnoremap <space> za
" copy to system clipboard
set clipboard=unnamed

""""""""""""
"  search  "
""""""""""""
set ignorecase
set smartcase
set hlsearch
set incsearch
"""""""""""
"  input  "
"""""""""""
" 自動縮排
set cindent
" Tab 字元寬度
" set tabstop=4
" 自動換行的縮行寬度
set shiftwidth=2
" Tab 使用shiftwidth 設定
set ts=2
set softtabstop=2
set textwidth=79
set expandtab
set autoindent
set smarttab
" backspace
set backspace=indent,eol,start " set backspace=2

